package org.geotools.map.opengl.features;

import java.awt.Color;

import org.geotools.map.opengl.RenderableFeature;
import org.geotools.styling.LineSymbolizer;
import org.geotools.styling.Stroke;
import org.locationtech.jts.geom.Coordinate;
import org.opengis.feature.Feature;
import org.opengis.referencing.operation.MathTransform;

import com.jogamp.opengl.GL2;

public abstract class LineFeature extends RenderableFeature {
    protected boolean hasStroke;

    protected LineFeature(Coordinate[] coordinates, Feature feature, MathTransform transform, LineSymbolizer symbolizer) {
        super(coordinates, feature, transform);
        this.hasStroke = symbolizer.getStroke() != null;
    }

    protected abstract float[] getStrokeColor();

    protected abstract float getStrokeWidth();

    @Override
    public void render(GL2 gl, float depth, float scale) {
        float[] colorFloats = getStrokeColor();
        float strokeWidth = getStrokeWidth();

        gl.glLineWidth(strokeWidth);
        gl.glColor4f(colorFloats[0], colorFloats[1], colorFloats[2], colorFloats[3]);
        gl.glBegin(GL2.GL_LINES);
        for (Coordinate coordinate : super.getCoordinates()) {
            gl.glVertex3f((float) coordinate.getX(), (float) coordinate.getY(), 0);
        }
        gl.glEnd();
    }

    public static LineFeature create(Coordinate[] coordinates, Feature feature, LineSymbolizer symbolizer, MathTransform transform) {
        return new DynamicLineFeature(coordinates, feature, transform, symbolizer);
    }

    private static class DynamicLineFeature extends LineFeature {
        private Stroke stroke;
        private Feature feature;

        protected DynamicLineFeature(Coordinate[] coordinates, Feature feature, MathTransform transform, LineSymbolizer symbolizer) {
            super(coordinates, feature, transform, symbolizer);
            this.feature = feature;
            this.stroke = symbolizer.getStroke();
        }

        @Override
        protected float[] getStrokeColor() {
            Color strokeColor = this.stroke.getColor().evaluate(feature, Color.class);

            float[] colorFloats = strokeColor.getRGBColorComponents(new float[4]);
            colorFloats[3] = this.stroke.getOpacity().evaluate(feature, float.class);

            return colorFloats;
        }

        @Override
        protected float getStrokeWidth() {
            float strokeWidth = this.stroke.getWidth().evaluate(feature, float.class);
            return strokeWidth;
        }

    }

}
