package org.geotools.map.opengl.features;

import java.awt.Color;

import javax.swing.Icon;

import org.geotools.map.opengl.RenderableFeature;
import org.geotools.map.opengl.util.RenderUtil;
import org.geotools.styling.PointSymbolizer;
import org.locationtech.jts.geom.Coordinate;
import org.opengis.feature.Feature;
import org.opengis.filter.expression.Expression;
import org.opengis.referencing.operation.MathTransform;
import org.opengis.style.ExternalGraphic;
import org.opengis.style.ExternalMark;
import org.opengis.style.Fill;
import org.opengis.style.Graphic;
import org.opengis.style.GraphicalSymbol;
import org.opengis.style.Mark;
import org.opengis.style.Stroke;

import com.jogamp.opengl.GL2;

public abstract class PointFeature extends RenderableFeature {

    protected Graphic graphic;

    protected PointFeature(Coordinate[] coordinates, Feature feature, MathTransform transform, PointSymbolizer symbolizer) {
        super(coordinates, feature, transform);
        this.graphic = symbolizer.getGraphic();
    }

    private static class MarkPointFeature extends PointFeature {
        private Feature feature;
        private Mark mark;
        private Fill fill;
        private Stroke stroke;
        private Expression sizeExpression, rotationExpression;

        protected MarkPointFeature(Coordinate[] coordinates, Feature feature, MathTransform transform, PointSymbolizer symbolizer, Mark mark) {
            super(coordinates, feature, transform, symbolizer);
            this.feature = feature;
            this.mark = mark;
            this.fill = mark.getFill();
            this.stroke = mark.getStroke();
            this.sizeExpression = super.graphic.getSize();
            this.rotationExpression = super.graphic.getRotation();
        }

        private Coordinate[] createCircle(float scale) {
            final int points = 32;
            final Coordinate[] coordinates = new Coordinate[points];

            final float size = getSize();
            final double angleIncrement = (2 * Math.PI) / points;
            double angle = 0;
            double x, y;
            final Coordinate coordinate = super.getCoordinate();
            for (int i = 0; i < points; i++, angle += angleIncrement) {
                y = coordinate.y + (Math.sin(angle) * size * scale);
                x = coordinate.x + (Math.cos(angle) * size * scale);
                coordinates[i] = new Coordinate(x, y);
            }

            return coordinates;
        }

        private Coordinate[] createCross(float scale) {
            final int points = 12;
            final Coordinate[] coordinates = new Coordinate[points];
            final Coordinate centerCoordinate = super.getCoordinate();
            final float size = getSize() * scale;

            double x, y;
            int i = 0;
            {
                x = 0;
                y = 0.2;
                x *= size;
                y *= size;
                x += centerCoordinate.x;
                y += centerCoordinate.y;
                coordinates[i++] = new Coordinate(x, y);
            }
            {
                x = 0.9;
                y = 1;
                x *= size;
                y *= size;
                x += centerCoordinate.x;
                y += centerCoordinate.y;
                coordinates[i++] = new Coordinate(x, y);
            }
            {
                x = 1;
                y = 0.9;
                x *= size;
                y *= size;
                x += centerCoordinate.x;
                y += centerCoordinate.y;
                coordinates[i++] = new Coordinate(x, y);
            }
            {
                x = 0.2;
                y = 0;
                x *= size;
                y *= size;
                x += centerCoordinate.x;
                y += centerCoordinate.y;
                coordinates[i++] = new Coordinate(x, y);
            }
            {
                x = 1;
                y = -0.9;
                x *= size;
                y *= size;
                x += centerCoordinate.x;
                y += centerCoordinate.y;
                coordinates[i++] = new Coordinate(x, y);
            }
            {
                x = 0.9;
                y = -1;
                x *= size;
                y *= size;
                x += centerCoordinate.x;
                y += centerCoordinate.y;
                coordinates[i++] = new Coordinate(x, y);
            }
            {
                x = 0;
                y = -0.2;
                x *= size;
                y *= size;
                x += centerCoordinate.x;
                y += centerCoordinate.y;
                coordinates[i++] = new Coordinate(x, y);
            }
            {
                x = -0.9;
                y = -1;
                x *= size;
                y *= size;
                x += centerCoordinate.x;
                y += centerCoordinate.y;
                coordinates[i++] = new Coordinate(x, y);
            }
            {
                x = -1;
                y = -0.9;
                x *= size;
                y *= size;
                x += centerCoordinate.x;
                y += centerCoordinate.y;
                coordinates[i++] = new Coordinate(x, y);
            }
            {
                x = -0.2;
                y = 0;
                x *= size;
                y *= size;
                x += centerCoordinate.x;
                y += centerCoordinate.y;
                coordinates[i++] = new Coordinate(x, y);
            }
            {
                x = -1;
                y = 0.9;
                x *= size;
                y *= size;
                x += centerCoordinate.x;
                y += centerCoordinate.y;
                coordinates[i++] = new Coordinate(x, y);
            }
            {
                x = -0.9;
                y = 1;
                x *= size;
                y *= size;
                x += centerCoordinate.x;
                y += centerCoordinate.y;
                coordinates[i++] = new Coordinate(x, y);
            }

            return coordinates;
        }

        private Coordinate[] createSquare(float scale) {
            final int points = 4;
            final Coordinate[] coordinates = new Coordinate[points];
            final Coordinate centerCoordinate = super.getCoordinate();
            final float size = getSize() * scale;

            double x, y;
            { // Top left
                x = centerCoordinate.x - size / 2;
                y = centerCoordinate.y + size / 2;
                coordinates[0] = new Coordinate(x, y);
            }
            { // Top right
                x = centerCoordinate.x + size / 2;
                y = centerCoordinate.y + size / 2;
                coordinates[1] = new Coordinate(x, y);
            }
            { // Bottom left
                x = centerCoordinate.x - size / 2;
                y = centerCoordinate.y - size / 2;
                coordinates[2] = new Coordinate(x, y);
            }
            { // bottom right
                x = centerCoordinate.x + size / 2;
                y = centerCoordinate.y - size / 2;
                coordinates[3] = new Coordinate(x, y);
            }

            return coordinates;
        }

        private Coordinate[] createStar(float scale) {
            final int points = 10;
            final Coordinate[] coordinates = new Coordinate[points];

            final float size = getSize();
            final double angleIncrement = (2*Math.PI) / points;
            double angle = 0;
            double x, y;

            final Coordinate coordinate = super.getCoordinate();
            
            for (int i = 0; i < points; i++) {
                double scale2 = (i % 2 != 0) ? 1 : 0.3;
                x = coordinate.x + (Math.sin(angle) * size * scale * scale2);
                y = coordinate.y + (Math.cos(angle) * size * scale * scale2);
                coordinates[i] = new Coordinate(x, y);
                angle += angleIncrement;
            }

            return coordinates;
        }

        private Coordinate[] createTriangle(float scale) {
            final Coordinate[] coordinates = new Coordinate[3];
            final Coordinate centerCoordinate = super.getCoordinate();
            float size = getSize();
            double x, y;
            { // Top point of triangle
                x = centerCoordinate.x;
                y = centerCoordinate.y + size * scale / 2;
                coordinates[0] = new Coordinate(x, y);
            }
            { // Left point of triangle
                x = centerCoordinate.x - size * scale / 2;
                y = centerCoordinate.y - size * scale / 2;
                coordinates[1] = new Coordinate(x, y);
            }
            { // Right point of triangle
                x = centerCoordinate.x + size * scale / 2;
                y = centerCoordinate.y - size * scale / 2;
                coordinates[2] = new Coordinate(x, y);
            }
            return coordinates;
        }

        private void drawFill(GL2 gl, Coordinate[] coordinates, float depth) {
            float[] colorFloats = getFillColorArray();

            RenderUtil.setColor(gl, colorFloats);
            RenderUtil.fillPolygon(gl, coordinates, depth);
        }

        private void drawStroke(GL2 gl, Coordinate[] coordinates, float depth, float scale) {
            float strokeWidth = getStrokeWidth();
            float[] colorFloats = getStrokeColorArray();
            
            RenderUtil.setLineWidth(gl, strokeWidth);
            RenderUtil.setColor(gl, colorFloats);
            RenderUtil.strokePolygon(gl, coordinates, depth+0.01f);
        }

        protected float[] getFillColorArray() {
            float[] fillColor = this.fill.getColor().evaluate(feature, Color.class).getRGBColorComponents(new float[4]);
            fillColor[3] = this.fill.getOpacity().evaluate(feature, float.class);
            return fillColor;
        }

        protected float getRotation() {
            return this.rotationExpression.evaluate(feature, float.class);
        }

        protected float getSize() {
            return this.sizeExpression.evaluate(feature, float.class);
        }

        protected float[] getStrokeColorArray() {
            float[] strokeColor = this.stroke.getColor().evaluate(feature, Color.class).getRGBColorComponents(new float[4]);
            strokeColor[3] = this.stroke.getOpacity().evaluate(feature, float.class);
            return strokeColor;
        }

        protected float getStrokeWidth() {
            return this.stroke.getWidth().evaluate(feature, float.class);
        }

        protected boolean hasFill() {
            return this.fill != null;
        }

        protected boolean hasStroke() {
            return this.stroke != null;
        }

        @Override
        public void render(GL2 gl, float depth, float scale) {
            final Coordinate[] coordinates;
            String wkn = null;
            final ExternalMark eMark = mark.getExternalMark();
            if (mark.getWellKnownName() != null) {
                wkn = mark.getWellKnownName().evaluate(feature, String.class);
            }
            if (wkn != null) {
                switch (wkn.toUpperCase()) {
                case "TRIANGLE":
                    coordinates = createTriangle(scale);
                    break;
                case "CIRCLE":
                    coordinates = createCircle(scale);
                    break;
                case "STAR":
                    coordinates = createStar(scale);
                    break;
                case "CROSS":
                case "X":
                    coordinates = createCross(scale);
                    break;
                case "SQUARE":
                default:
                    coordinates = createSquare(scale);
                    break;
                }
                
                if (this.hasFill()) {
                    drawFill(gl, coordinates, depth);
                }
                if (this.hasStroke()) {
                    drawStroke(gl, coordinates, depth, scale);
                }
            } else if (eMark != null) {
                Icon icon = eMark.getInlineContent();
            }
        }
    }

    public static RenderableFeature create(Coordinate[] coordinates, Feature feature, PointSymbolizer symbolizer, MathTransform transform) {
        for (GraphicalSymbol symbol : symbolizer.getGraphic().graphicalSymbols()) {
            if (symbol instanceof ExternalGraphic) {
                ExternalGraphic eg = (ExternalGraphic) symbol;

            } else if (symbol instanceof Mark) {
                Mark mark = (Mark) symbol;
                return new MarkPointFeature(coordinates, feature, transform, symbolizer, mark);
            }
        }
        return null;
    }

}
