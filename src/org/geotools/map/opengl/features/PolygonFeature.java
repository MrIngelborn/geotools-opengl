package org.geotools.map.opengl.features;

import java.awt.Color;
import java.util.ArrayList;

import org.geotools.map.opengl.RenderableFeature;
import org.geotools.map.opengl.util.RenderUtil;
import org.geotools.styling.Fill;
import org.geotools.styling.PolygonSymbolizer;
import org.geotools.styling.Stroke;
import org.locationtech.jts.geom.Coordinate;
import org.opengis.feature.Feature;
import org.opengis.filter.expression.BinaryExpression;
import org.opengis.filter.expression.Expression;
import org.opengis.filter.expression.Literal;
import org.opengis.referencing.operation.MathTransform;

import com.jogamp.opengl.GL2;

public abstract class PolygonFeature extends RenderableFeature {
    protected boolean hasFill, hasStroke;

    public PolygonFeature(Coordinate[] coordinates, Feature feature, MathTransform transform, PolygonSymbolizer symbolizer) {
        super(coordinates, feature, transform);
        this.hasFill = symbolizer.getFill() != null;
        this.hasStroke = symbolizer.getStroke() != null;
    }

    @Override
    public void render(GL2 gl, float depth, float scale) {
        if (this.hasFill) {
            drawFill(gl, depth);
        }
        if (this.hasStroke) {
            drawStroke(gl, depth);
        }
    }

    private void drawStroke(GL2 gl, float depth) {
        float strokeWidth = getStrokeWidth();
        float[] colorFloats = getStrokeColorArray();
        
        RenderUtil.setLineWidth(gl, strokeWidth);
        RenderUtil.setColor(gl, colorFloats);
        RenderUtil.strokePolygon(gl, super.getCoordinates(), depth+0.01f);
    }

    protected abstract float[] getStrokeColorArray();

    protected abstract float getStrokeWidth();

    private void drawFill(GL2 gl, float depth) {
        float[] colorFloats = getFillColorArray();

        RenderUtil.setColor(gl, colorFloats);
        RenderUtil.fillPolygon(gl, super.getCoordinates(), depth);
    }

    protected abstract float[] getFillColorArray();

    public static PolygonFeature create(Coordinate[] coordinates, Feature feature, PolygonSymbolizer symbolizer, MathTransform transform) {
        if (symboliserIsStatic(symbolizer)) {
            return new StaticPolygonFeature(coordinates, feature, transform, symbolizer);
        }
        return new DynamicPolygonFeature(coordinates, feature, transform, symbolizer);
    }

    private static boolean symboliserIsStatic(PolygonSymbolizer symbolizer) {
        final ArrayList<Expression> expressions = new ArrayList<>();

        // Expressions to evaluate
        if (symbolizer.getFill() != null) {
            expressions.add(symbolizer.getFill().getColor());
            expressions.add(symbolizer.getFill().getOpacity());
        }
        if (symbolizer.getStroke() != null) {
            expressions.add(symbolizer.getStroke().getColor());
            expressions.add(symbolizer.getStroke().getOpacity());
            expressions.add(symbolizer.getStroke().getWidth());
        }

        // Evaluate expressions
        for (Expression expression : expressions) {
            if (!expressionIsLitteral(expression)) {
                return false;
            }
        }
        return true;
    }

    private static boolean expressionIsLitteral(Expression expression) {
        // TODO: Missing some type of expression?
        if (expression instanceof Literal)
            return true;
        if (expression instanceof BinaryExpression) {
            if (!expressionIsLitteral(((BinaryExpression) expression).getExpression1()))
                return false;
            if (!expressionIsLitteral(((BinaryExpression) expression).getExpression2()))
                return false;
            return true;
        }
        return false;
    }

    private static class StaticPolygonFeature extends PolygonFeature {
        private float[] fillColor, strokeColor;
        private float strokeWidth;

        public StaticPolygonFeature(Coordinate[] coordinates, Feature feature, MathTransform transform, PolygonSymbolizer symbolizer) {
            super(coordinates, feature, transform, symbolizer);
            if (this.hasFill) {
                this.fillColor = symbolizer.getFill().getColor().evaluate(null, Color.class).getRGBColorComponents(new float[4]);
                this.fillColor[3] = symbolizer.getFill().getOpacity().evaluate(null, float.class);
            }
            if (this.hasStroke) {
                this.strokeColor = symbolizer.getFill().getColor().evaluate(null, Color.class).getRGBColorComponents(new float[4]);
                this.strokeColor[3] = symbolizer.getStroke().getOpacity().evaluate(null, float.class);
                this.strokeWidth = symbolizer.getStroke().getWidth().evaluate(null, float.class);
            }
        }

        @Override
        protected float[] getStrokeColorArray() {
            return strokeColor;
        }

        @Override
        protected float getStrokeWidth() {
            return this.strokeWidth;
        }

        @Override
        protected float[] getFillColorArray() {
            return this.fillColor;
        }

    }

    private static class DynamicPolygonFeature extends PolygonFeature {
        private Feature feature;
        private Fill fill;
        private Stroke stroke;

        public DynamicPolygonFeature(Coordinate[] coordinates, Feature feature, MathTransform transform, PolygonSymbolizer symbolizer) {
            super(coordinates, feature, transform, symbolizer);
            this.feature = feature;
        }

        @Override
        protected float[] getStrokeColorArray() {
            Color color = this.stroke.getColor().evaluate(this.feature, Color.class);
            float opacity = this.stroke.getOpacity().evaluate(this.feature, float.class);

            float[] colorArray = { 0, 0, 0, opacity };
            colorArray = color.getRGBColorComponents(colorArray);

            return colorArray;
        }

        @Override
        protected float getStrokeWidth() {
            return stroke.getWidth().evaluate(this.feature, float.class);
        }

        @Override
        protected float[] getFillColorArray() {
            Color color = this.fill.getColor().evaluate(this.feature, Color.class);
            float opacity = this.fill.getOpacity().evaluate(this.feature, float.class);

            float[] colorArray = { 0, 0, 0, opacity };
            colorArray = color.getRGBColorComponents(colorArray);

            return colorArray;
        }

    }

}
