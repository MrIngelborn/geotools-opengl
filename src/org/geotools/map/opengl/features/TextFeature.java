package org.geotools.map.opengl.features;

import org.geotools.map.opengl.RenderableFeature;
import org.geotools.styling.Font;
import org.geotools.styling.TextSymbolizer;
import org.locationtech.jts.geom.Coordinate;
import org.opengis.feature.Feature;
import org.opengis.referencing.operation.MathTransform;

import com.jogamp.opengl.GL2;

public abstract class TextFeature extends RenderableFeature {
    private Font font;

    private TextFeature(Coordinate[] coordinates, Feature feature, MathTransform transform, TextSymbolizer textSymbolizer) {
        super(coordinates, feature, transform);
        this.font = textSymbolizer.getFont();
        textSymbolizer.getLabel();
        textSymbolizer.getHalo();
        textSymbolizer.getFill();
        // TODO Auto-generated constructor stub
    }

    @Override
    public void render(GL2 gl, float depth, float scale) {
        // TODO Auto-generated method stub

    }

    public static TextFeature create(Coordinate[] coordinates, Feature feature, TextSymbolizer textSymbolizer, MathTransform transform) {
        return new DynamicTextFeature(coordinates, feature, transform, textSymbolizer);
    }

    private static class DynamicTextFeature extends TextFeature {
        public DynamicTextFeature(Coordinate[] coordinates, Feature feature, MathTransform transform, TextSymbolizer textSymbolizer) {
            super(coordinates, feature, transform, textSymbolizer);
        }
    }

}
