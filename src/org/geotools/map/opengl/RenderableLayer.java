package org.geotools.map.opengl;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.geotools.data.FeatureEvent;
import org.geotools.data.FeatureListener;
import org.geotools.data.FeatureSource;
import org.geotools.data.Query;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.geotools.feature.FeatureTypes;
import org.geotools.filter.IllegalFilterException;
import org.geotools.filter.spatial.DefaultCRSFilterVisitor;
import org.geotools.filter.spatial.ReprojectingFilterVisitor;
import org.geotools.filter.visitor.SpatialFilterVisitor;
import org.geotools.map.Layer;
import org.geotools.map.MapContent;
import org.geotools.map.MapLayerEvent;
import org.geotools.map.MapLayerListener;
import org.geotools.referencing.CRS;
import org.geotools.styling.FeatureTypeStyle;
import org.geotools.styling.Rule;
import org.geotools.styling.Symbolizer;
import org.locationtech.jts.geom.Geometry;
import org.opengis.feature.Feature;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.type.FeatureType;
import org.opengis.feature.type.GeometryDescriptor;
import org.opengis.feature.type.Name;
import org.opengis.feature.type.PropertyDescriptor;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;
import org.opengis.filter.expression.Expression;
import org.opengis.filter.expression.PropertyName;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.NoSuchAuthorityCodeException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;

import com.jogamp.opengl.GL2;

public class RenderableLayer {
	/** Filter factory for creating bounding box filters */
	private static final FilterFactory2 filterFactory = CommonFactoryFinder.getFilterFactory2(null);
	private static final boolean DEBUG = false;
	private MapContent mapContent;
	private Layer layer;
	private LayerListener listener = new LayerListener();
	private FeatureListener featureListener = new LayerFeatureListener();
	private float depth;

	private Map<Feature, List<RenderableFeature>> featureMap = new HashMap<>();
	private MathTransform transform;

	private List<FeatureTypeStyle> activeFTSs;

	public RenderableLayer(MapContent mapContent, Layer layer, float depth) {
		if (layer == null) {
			throw new IllegalArgumentException("Layer must not be null");
		}

		this.mapContent = mapContent;
		this.layer = layer;
		this.depth = depth;

		init();
	}

	/**
	 * Checks the attributes in the query (which we got from the SLD) match the
	 * schema, throws an {@link IllegalFilterException} otherwise
	 *
	 * @param schema
	 * @param attributeNames
	 */
	private void checkAttributeExistence(FeatureType schema, Query query) {
		if (query.getProperties() == null) {
			return;
		}

		for (PropertyName attribute : query.getProperties()) {
			if (attribute.evaluate(schema) == null) {
				if (schema instanceof SimpleFeatureType) {
					List<Name> allNames = new ArrayList<Name>();
					for (PropertyDescriptor pd : schema.getDescriptors()) {
						allNames.add(pd.getName());
					}
					throw new IllegalFilterException("Could not find '" + attribute + "' in the FeatureType ("
							+ schema.getName() + "), available attributes are: " + allNames);
				} else {
					throw new IllegalFilterException(
							"Could not find '" + attribute + "' in the FeatureType (" + schema.getName() + ")");
				}
			}
		}
	}

	private void createRenderableFeatures(Feature feature) {
		ArrayList<RenderableFeature> renderableFeatures = new ArrayList<>();
		for (FeatureTypeStyle fts : activeFTSs) {
			for (Rule rule : fts.rules()) {
				final Filter filter = rule.getFilter();
				if (filter == null || filter.evaluate(feature)) {
					List<RenderableFeature> symbolizerFeatures = createRenderableFeaturesFromSymbolizers(feature,
							rule.symbolizers());
					renderableFeatures.addAll(symbolizerFeatures);
				}
			}
		}

		this.featureMap.put(feature, renderableFeatures);
	}

	private List<RenderableFeature> createRenderableFeaturesFromSymbolizers(Feature feature,
			List<Symbolizer> symbolizers) {
		final MathTransform transform = getTransform();
		ArrayList<RenderableFeature> renderableFeatures = new ArrayList<>();
		for (Symbolizer symbolizer : symbolizers) {
			Geometry geometry = findGeometry(feature, symbolizer);
			Collection<RenderableFeature> symbolFeatures = RenderableFeature.create(feature, geometry, symbolizer,
					transform);
			renderableFeatures.addAll(symbolFeatures);
		}
		return renderableFeatures;
	}

	public void dispose() {
		this.layer.removeMapLayerListener(listener);
		this.transform = null;
	}

	/**
	 * Finds the geometric attribute requested by the symbolizer
	 * 
	 * @param feature    The feature
	 * @param symbolizer The symbolizer
	 * @return The geometry requested in the symbolizer, or the default geometry if
	 *         none is specified
	 */
	private org.locationtech.jts.geom.Geometry findGeometry(Feature feature, Symbolizer symbolizer) {
		Expression geomExpr = symbolizer.getGeometry();

		// get the geometry
		Geometry geom;
		if (geomExpr == null) {
			if (feature instanceof SimpleFeature) {
				geom = (Geometry) ((SimpleFeature) feature).getDefaultGeometry();
			} else {
				geom = (Geometry) feature.getDefaultGeometryProperty().getValue();
				// } else {
				// geom = defaultGeometryPropertyName.evaluate(drawMe, Geometry.class);
			}
		} else {
			geom = geomExpr.evaluate(feature, Geometry.class);
		}

		return geom;
	}

	/**
	 * Computes the declared SRS of a layer based on the layer schema and the EPSG
	 * forcing flag
	 *
	 * @param schema
	 * @return
	 * @throws FactoryException
	 * @throws NoSuchAuthorityCodeException
	 */
	private CoordinateReferenceSystem getDeclaredSRS(FeatureType schema) {
		// compute the default SRS of the feature source
		CoordinateReferenceSystem declaredCRS = schema.getCoordinateReferenceSystem();
		// if (isEPSGAxisOrderForced()) {
		// Integer code = CRS.lookupEpsgCode(declaredCRS, false);
		// if (code != null) {
		// declaredCRS = CRS.decode("urn:ogc:def:crs:EPSG::" + code);
		// }
		// }
		return declaredCRS;
	}

	private Query getDefinitionQuery(Layer layer, FeatureSource<? extends FeatureType, ? extends Feature> featureSource,
			CoordinateReferenceSystem sourceCrs) {
		// now, if a definition query has been established for this layer, be
		// sure to respect it by combining it with the bounding box one.
		Query definitionQuery = new Query(reprojectQuery(layer.getQuery(), featureSource));
		definitionQuery.setCoordinateSystem(sourceCrs);

		return definitionQuery;
	}

	private FeatureCollection<?, ?> getFeatures(final Layer layer, final FeatureType schema,
			List<FeatureTypeStyle> featureTypeStyles) {

		final FeatureSource<? extends FeatureType, ? extends Feature> featureSource = layer.getFeatureSource();

		// grab the source crs and geometry attribute
		final CoordinateReferenceSystem sourceCrs;
		final GeometryDescriptor geometryAttribute = schema.getGeometryDescriptor();
		if (geometryAttribute != null && geometryAttribute.getType() != null) {
			sourceCrs = geometryAttribute.getType().getCoordinateReferenceSystem();
		} else {
			sourceCrs = null;
		}

		// ... assume we have to do the generalization, the query layer process will
		// turn down the flag if we don't
		// boolean hasTransformation = transform != null;
		// Query styleQuery = getStyleQuery(
		// layer,
		// featureTypeStyles,
		// mapExtent,
		// destinationCrs,
		// sourceCrs,
		// screenSize,
		// geometryAttribute,
		// worldToScreenTransform,
		// hasTransformation);
		Query definitionQuery = getDefinitionQuery(layer, featureSource, sourceCrs);
		FeatureCollection<?, ?> features = null;
		// Query mixed = DataUtilities.mixQueries(definitionQuery, styleQuery, null);
		// mix the sort by, the style query takes precedence
		// if (styleQuery.getSortBy() != null) {
		// mixed.setSortBy(styleQuery.getSortBy());
		// } else {
		// mixed.setSortBy(definitionQuery.getSortBy());
		// }
		checkAttributeExistence(featureSource.getSchema(), definitionQuery);
		try {
			features = featureSource.getFeatures();
		} catch (IOException e) {
		}
		// features = RendererUtilities.fixFeatureCollectionReferencing(features,
		// sourceCrs);

		return features;
	}

	private MathTransform getTransform() {
		if (this.transform == null) {
			final FeatureSource<? extends FeatureType, ? extends Feature> featureSource = this.layer.getFeatureSource();
			if (featureSource != null) {
				final FeatureType featureType = featureSource.getSchema();
				final CoordinateReferenceSystem featureTypeCrs = featureType.getCoordinateReferenceSystem();
				CoordinateReferenceSystem mapCrs = mapContent.getCoordinateReferenceSystem();
				if (mapCrs == null) {
					mapCrs = mapContent.layers().get(0).getFeatureSource().getSchema().getCoordinateReferenceSystem();
				}
				if (!featureTypeCrs.equals(mapCrs)) {
					try {
						transform = CRS.findMathTransform(featureType.getCoordinateReferenceSystem(),
								this.mapContent.getCoordinateReferenceSystem(), true);
					} catch (FactoryException e) {
						throw new RuntimeException(e);
					}
				}
			}
		}
		return this.transform;
	}

	public void init() {
		final FeatureSource<? extends FeatureType, ? extends Feature> featureSource = this.layer.getFeatureSource();
		if (featureSource == null) {
			throw new IllegalArgumentException("The layer does not contain a feature source: " + this.layer.getTitle());
		}
		featureSource.addFeatureListener(featureListener);

		final FeatureType featureType = featureSource.getSchema();

		// Get relevant feature type styles
		this.activeFTSs = new ArrayList<>();
		for (FeatureTypeStyle fts : layer.getStyle().featureTypeStyles()) {
			if (isFeatureTypeStyleActive(featureType, fts)) {
				activeFTSs.add(fts);
			}
		}

		// Get all features to be processed
		FeatureCollection<? extends FeatureType, ? extends Feature> featureCollection = getFeatures(layer, featureType,
				activeFTSs);

		// Process features
		FeatureIterator<? extends Feature> features = featureCollection.features();
		while (features.hasNext()) {
			Feature feature = features.next();
			createRenderableFeatures(feature);
		}
	}

	private boolean isFeatureTypeStyleActive(FeatureType ftype, FeatureTypeStyle fts) {
		final boolean styleHasNoNames = fts.featureTypeNames().isEmpty();
		final boolean typeHasName = ftype.getName().getLocalPart() != null;

		if (styleHasNoNames) {
			return true;
		}

		if (typeHasName) {
			final String typeNameLocal = ftype.getName().getLocalPart();
			for (Name name : fts.featureTypeNames()) {
				final String nameLocal = name.getLocalPart();
				if (typeNameLocal.equalsIgnoreCase(nameLocal)) {
					return true;
				}
				final boolean isDecendant = FeatureTypes.isDecendedFrom(ftype, URI.create(name.getNamespaceURI()),
						name.getLocalPart());
				if (isDecendant) {
					return true;
				}
			}
		}

		return false;
	}

	public void render(GL2 gl, float scale) {
		if (DEBUG) System.out.printf("\tStarting layer: %s at depth %.2f\n", this.layer.getTitle(), depth);
		if (!this.layer.isVisible()) {
			if (DEBUG) System.out.println("\t\tSkipping invisible");
			return;
		}

		if (DEBUG) System.out.printf("\t\tRendering %d features\n", featureMap.size());

		for (List<RenderableFeature> featureList : this.featureMap.values()) {
			for (RenderableFeature feature : featureList) {
				feature.render(gl, depth, scale);
			}
		}
		if (DEBUG) System.out.println("\t\tDone!");
	}

	/**
	 * Reprojects all spatial filters in the specified Query so that they match the
	 * native srs of the specified feature source.
	 */
	private Query reprojectQuery(Query query, FeatureSource<? extends FeatureType, ? extends Feature> source) {
		if (query == null || query.getFilter() == null) {
			return query;
		}

		// compute the declared CRS
		Filter original = query.getFilter();
		CoordinateReferenceSystem declaredCRS = getDeclaredSRS(source.getSchema());
		Filter reprojected = reprojectSpatialFilter(declaredCRS, source.getSchema(), original);
		if (reprojected == original) {
			return query;
		} else {
			Query rq = new Query(query);
			rq.setFilter(reprojected);
			return rq;
		}
	}

	/**
	 * Reprojects spatial filters so that they match the feature source native CRS,
	 * and assuming all literal geometries are specified in the specified
	 * declaredCRS
	 */
	private Filter reprojectSpatialFilter(CoordinateReferenceSystem declaredCRS, FeatureType schema, Filter filter) {
		// NPE avoidance
		if (filter == null) {
			return null;
		}

		// do we have any spatial filter?
		SpatialFilterVisitor sfv = new SpatialFilterVisitor();
		filter.accept(sfv, null);
		if (!sfv.hasSpatialFilter()) {
			return filter;
		}

		// all right, we need to default the literals to the declaredCRS and then
		// reproject to
		// the native one
		DefaultCRSFilterVisitor defaulter = new DefaultCRSFilterVisitor(filterFactory, declaredCRS);
		Filter defaulted = (Filter) filter.accept(defaulter, null);
		ReprojectingFilterVisitor reprojector = new ReprojectingFilterVisitor(filterFactory, schema);
		Filter reprojected = (Filter) defaulted.accept(reprojector, null);
		return reprojected;
	}

	public void setDepth(float depth) {
		this.depth = depth;

		// TODO: remake model?
	}

	private class LayerFeatureListener implements FeatureListener {

		@Override
		public void changed(FeatureEvent featureEvent) {
			final FeatureEvent.Type eventType = featureEvent.getType();
			final Filter filter = featureEvent.getFilter();
			final Object source = featureEvent.getSource();

			FeatureCollection<?, ?> featureCollection = null;
			if (source instanceof FeatureSource<?, ?>) {
				FeatureSource<? extends FeatureType, ? extends Feature> featureSource;
				featureSource = (FeatureSource<?, ?>) featureEvent.getSource();
				try {
					featureCollection = featureSource.getFeatures(filter);
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}
			else if (source instanceof FeatureCollection<?, ?>) {
				featureCollection = ((FeatureCollection<?, ?>) source).subCollection(filter);
			}
			if (featureCollection != null) {
				FeatureIterator<? extends Feature> featureIterator = featureCollection.features();
				
				while (featureIterator.hasNext()) {
					Feature feature = featureIterator.next();
					switch (eventType) {
					case ADDED:
					case CHANGED:
						RenderableLayer.this.createRenderableFeatures(feature);
						break;
					case REMOVED:
						RenderableLayer.this.featureMap.remove(feature);
						break;
					default:
						break;
					}
				}
				
				
			}
		}

	}

	private class LayerListener implements MapLayerListener {

		@Override
		public void layerChanged(MapLayerEvent event) {
			// TODO Auto-generated method stub
			int reason = event.getReason();

			switch (reason) {
			case MapLayerEvent.STYLE_CHANGED:
			case MapLayerEvent.DATA_CHANGED:
				init();
				break;
			default:
				break;
			}
		}

		@Override
		public void layerDeselected(MapLayerEvent event) {
			// TODO Auto-generated method stub

		}

		@Override
		public void layerHidden(MapLayerEvent event) {
			// TODO Auto-generated method stub

		}

		@Override
		public void layerPreDispose(MapLayerEvent event) {
			// TODO Auto-generated method stub

		}

		@Override
		public void layerSelected(MapLayerEvent event) {
			// TODO Auto-generated method stub

		}

		@Override
		public void layerShown(MapLayerEvent event) {
			// TODO Auto-generated method stub

		}

	}

}
