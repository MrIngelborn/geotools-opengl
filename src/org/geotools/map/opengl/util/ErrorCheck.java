package org.geotools.map.opengl.util;

import static com.jogamp.opengl.GL.GL_NO_ERROR;

import com.jogamp.opengl.GL2;

public class ErrorCheck {

	public static void check(GL2 gl, String origin) {
		int error = gl.glGetError();
		if (error != GL_NO_ERROR) {
			throw new OpenGLError(error, origin);
		}
	}

}
