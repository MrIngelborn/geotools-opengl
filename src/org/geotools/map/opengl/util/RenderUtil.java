package org.geotools.map.opengl.util;

import org.locationtech.jts.geom.Coordinate;

import com.jogamp.opengl.GL2;

public class RenderUtil {
	private RenderUtil() {
	}

	public static void setColor(GL2 gl, float[] color) {
		if (color.length > 3) {
			gl.glColor4f(color[0], color[1], color[2], color[3]);
			ErrorCheck.check(gl, "glColor4f");
		} else if (color.length > 2) {
			gl.glColor3f(color[0], color[1], color[3]);
			ErrorCheck.check(gl, "glColor3f");
		}
	}

	public static void fillPolygon(GL2 gl, Coordinate[] coordinates, float depth) {
		renderPolygon(gl, coordinates, GL2.GL_FILL, depth);
	}

	public static void strokePolygon(GL2 gl, Coordinate[] coordinates, float depth) {
		renderLineLoop(gl, coordinates, depth);
	}

	public static void renderPolygon(GL2 gl, Coordinate[] coordinates, int polygonMode, float depth) {
		gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, polygonMode);
		ErrorCheck.check(gl, "glPolygonMode");
		render(gl, coordinates, GL2.GL_POLYGON, depth);
	}
	
	public static void renderLineLoop(GL2 gl, Coordinate[] coordinates, float depth) {
		render(gl, coordinates, GL2.GL_LINE_LOOP, depth);
	}

	private static void render(GL2 gl, Coordinate[] coordinates, int mode, float depth) {
		gl.glBegin(mode);

		float x, y;
		for (Coordinate coordinate : coordinates) {
			x = (float) coordinate.x;
			y = (float) coordinate.y;
			gl.glVertex3f(x, y, depth);
		}

		gl.glEnd();
		ErrorCheck.check(gl, "glEnd");
	}

	public static void setLineWidth(GL2 gl, float strokeWidth) {
		gl.glLineWidth(strokeWidth);
		ErrorCheck.check(gl, "glLineWidth");
	}
}
