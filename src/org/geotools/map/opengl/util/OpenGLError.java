package org.geotools.map.opengl.util;

import static com.jogamp.opengl.GL.GL_INVALID_ENUM;
import static com.jogamp.opengl.GL.GL_INVALID_FRAMEBUFFER_OPERATION;
import static com.jogamp.opengl.GL.GL_INVALID_OPERATION;
import static com.jogamp.opengl.GL.GL_INVALID_VALUE;
import static com.jogamp.opengl.GL.GL_OUT_OF_MEMORY;

public class OpenGLError extends Error {
	private static final long serialVersionUID = -8362494391028486049L;
	
	public OpenGLError(int error, String origin) {
		super("OpenGL Error ( " + parseErrorValue(error) + " ): " + origin);
	}
	
	private static String parseErrorValue(int error) {
		String errorString;
		switch (error) {
		case GL_INVALID_ENUM:
			errorString = "GL_INVALID_ENUM";
			break;
		case GL_INVALID_VALUE:
			errorString = "GL_INVALID_VALUE";
			break;
		case GL_INVALID_OPERATION:
			errorString = "GL_INVALID_OPERATION";
			break;
		case GL_INVALID_FRAMEBUFFER_OPERATION:
			errorString = "GL_INVALID_FRAMEBUFFER_OPERATION";
			break;
		case GL_OUT_OF_MEMORY:
			errorString = "GL_OUT_OF_MEMORY";
			break;
		default:
			errorString = "unknown error";
			break;
		}
		return errorString;
	}
	
}
