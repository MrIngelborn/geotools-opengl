package org.geotools.map.opengl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.geotools.geometry.DirectPosition2D;
import org.geotools.map.opengl.features.LineFeature;
import org.geotools.map.opengl.features.PointFeature;
import org.geotools.map.opengl.features.PolygonFeature;
import org.geotools.map.opengl.features.TextFeature;
import org.geotools.styling.LineSymbolizer;
import org.geotools.styling.PointSymbolizer;
import org.geotools.styling.PolygonSymbolizer;
import org.geotools.styling.Symbolizer;
import org.geotools.styling.TextSymbolizer;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryCollection;
import org.locationtech.jts.geom.Polygon;
import org.opengis.feature.Feature;
import org.opengis.geometry.MismatchedDimensionException;
import org.opengis.referencing.operation.MathTransform;
import org.opengis.referencing.operation.TransformException;

import com.jogamp.opengl.GL2;

public abstract class RenderableFeature {
	protected Coordinate[] coordinates;
    private Feature feature;
    private MathTransform transform;

    protected RenderableFeature(Coordinate[] coordinates, Feature feature, MathTransform transform) {
        this.feature = feature;
        this.transform = transform;
        this.coordinates = coordinates;
    }

    protected Geometry getGeometry() {
        return (Geometry) feature.getProperty(feature.getDefaultGeometryProperty().getName()).getValue();
    }

    protected Coordinate[] getCoordinates() {
        final Coordinate[] coordinates = this.getGeometry().getCoordinates();
        if (this.transform == null) return coordinates;
        
        final int coordCount = coordinates.length;
        final Coordinate[] transformedCoords = new Coordinate[coordCount];
        for (int i = 0; i < coordCount; i++) {
            transformedCoords[i] = transformCoordinate(coordinates[i]);
        }
        return coordinates;
    }

    protected Coordinate getCoordinate() {
        return transformCoordinate(getGeometry().getCoordinate());
    }

    private Coordinate transformCoordinate(Coordinate coordinate) {
        DirectPosition2D pos = new DirectPosition2D(coordinate.x, coordinate.y);
        try {
            transform.transform(pos, pos);
        } catch (MismatchedDimensionException | TransformException e) {
            throw new RuntimeException(e);
        }
        
        Coordinate transformed = new Coordinate(pos.x, pos.y);

        return transformed;
    }

    public abstract void render(GL2 gl, float depth, float scale);

    public static Collection<RenderableFeature> create(Feature feature, Geometry geometry, Symbolizer symbolizer, MathTransform transform) {
    	Coordinate[] coordinates = geometry.getCoordinates();
    	
        if (symbolizer instanceof LineSymbolizer) {
            return Collections.singleton(LineFeature.create(coordinates, feature, (LineSymbolizer) symbolizer, transform));
        }

        if (geometry instanceof GeometryCollection) {
            GeometryCollection collection = (GeometryCollection) geometry;
            final int subGeomCount = collection.getNumGeometries();
            Collection<RenderableFeature> features = new ArrayList<>(subGeomCount);
            for (int i = 0; i < subGeomCount; i++) {
                Geometry subgeom = collection.getGeometryN(i);
                Collection<RenderableFeature> subFeatures = create(feature, subgeom, symbolizer, transform);
                features.addAll(subFeatures);
            }
            return features;
        }

        if (symbolizer instanceof PolygonSymbolizer) {
            if (geometry instanceof Polygon) {
                return Collections.singleton(PolygonFeature.create(coordinates, feature, (PolygonSymbolizer) symbolizer, transform));
            }
        }

        if (symbolizer instanceof PointSymbolizer) {
            return Collections.singleton(PointFeature.create(coordinates, feature, (PointSymbolizer) symbolizer, transform));
        }

        if (symbolizer instanceof TextSymbolizer) {
            return Collections.singleton(TextFeature.create(coordinates, feature, (TextSymbolizer) symbolizer, transform));
        }

        System.err.println("Missing RenderableFeature for type:" + geometry.getGeometryType());
        return Collections.emptyList();
    }

}
