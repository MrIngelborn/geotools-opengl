package org.geotools.map.opengl;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Rectangle;
import java.awt.event.ComponentListener;
import java.awt.event.HierarchyBoundsAdapter;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.util.Collection;
import java.util.HashSet;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.swing.event.MouseInputAdapter;

import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.map.Layer;
import org.geotools.map.MapBoundsEvent;
import org.geotools.map.MapBoundsListener;
import org.geotools.map.MapContent;
import org.geotools.map.MapLayerEvent;
import org.geotools.map.MapLayerListEvent;
import org.geotools.map.MapLayerListListener;
import org.geotools.map.MapViewport;
import org.geotools.swing.MapPane;
import org.geotools.swing.MouseDragBox;
import org.geotools.swing.event.DefaultMapMouseEventDispatcher;
import org.geotools.swing.event.MapMouseEventDispatcher;
import org.geotools.swing.event.MapMouseListener;
import org.geotools.swing.event.MapPaneEvent;
import org.geotools.swing.event.MapPaneKeyHandler;
import org.geotools.swing.event.MapPaneListener;
import org.geotools.swing.tool.CursorTool;
import org.opengis.geometry.Envelope;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLCapabilitiesImmutable;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLJPanel;
import com.jogamp.opengl.util.Animator;

public abstract class GLMapPane extends GLJPanel
        implements MapPane, MapLayerListListener, MapBoundsListener {
    private static final long serialVersionUID = -8801265983503603695L;

    private static final GLProfile deafultProfile = GLProfile.get(GLProfile.GL2);
    /*
     * This field is used to cache the full extent of the combined map
     * layers.
     */
    private ReferencedEnvelope fullExtent;

    private CursorTool currentCursorTool;

    private AtomicBoolean clearLabelCache = new AtomicBoolean(true);
    private MapMouseEventDispatcher mouseEventDispatcher = new DefaultMapMouseEventDispatcher(this);
    private final Collection<MapPaneListener> mapPaneListeners = new HashSet<>();
    private final MouseDragBox dragBox = new MouseDragBox(this);
    private final KeyListener keyHandler = new MapPaneKeyHandler(this);

    private final ReadWriteLock paramsLock = new ReentrantReadWriteLock();
    private Renderer renderer;
    protected MapContent mapContent;

    private ReferencedEnvelope pendingDisplayArea;

	private Animator animator;

    public GLMapPane() {
        this(new GLCapabilities(GLMapPane.deafultProfile));
        
        animator = new Animator(this);
        animator.start();
    }

    public GLMapPane(GLCapabilitiesImmutable capabilities) {
        super(capabilities);

        this.renderer = new Renderer();
        this.addGLEventListener(this.renderer);

        super.addMouseListener(this.mouseEventDispatcher);
        super.addMouseMotionListener(this.mouseEventDispatcher);
        super.addMouseWheelListener(this.mouseEventDispatcher);

        super.addMouseListener(this.dragBox);
        super.addMouseMotionListener(this.dragBox);

        /*
         * Listen for mouse entered events to (re-)set the
         * current tool cursor, otherwise the cursor seems to
         * default to the standard cursor sometimes (at least
         * on OSX)
         */
        super.addMouseListener(
                new MouseInputAdapter() {
                    @Override
                    public void mouseEntered(MouseEvent e) {
                        super.mouseEntered(e);
                        if (GLMapPane.this.currentCursorTool != null) {
                            GLMapPane.this.setCursor(GLMapPane.this.currentCursorTool.getCursor());
                        }
                    }
                });

        super.addKeyListener(this.keyHandler);

        /*
         * Note: we listen for both resizing events (with HierarchyBoundsListener)
         * and showing events (with HierarchyListener). Although showing
         * is often accompanied by resizing this is not reliable in Swing.
         */
        addHierarchyListener(
                new HierarchyListener() {
                    @Override
                    public void hierarchyChanged(HierarchyEvent he) {
                        if ((he.getChangeFlags() & HierarchyEvent.SHOWING_CHANGED) != 0) {
                            if (GLMapPane.this.isShowing()) {
                                GLMapPane.this.setForNewSize();
                            }
                        }
                    }
                });

        addHierarchyBoundsListener(
                new HierarchyBoundsAdapter() {
                    @Override
                    public void ancestorResized(HierarchyEvent he) {
                        if (isShowing()) {
                            GLMapPane.this.setForNewSize();
                        }
                    }
                });
    }

    public void dispose() {
    	this.animator.stop();
    }
    
    protected void setForNewSize() {
        if (mapContent != null) {

            /*
             * Compare the new pane screen size to the viewport's screen area
             * and skip further action if the two rectangles are equal. This
             * check avoid extra rendering requests when redundant resize events
             * are received (e.g. on mouse button release after drag resizing).
             */
            if (mapContent.getViewport().getScreenArea().equals(getVisibleRect())) {
                return;
            }

            mapContent.getViewport().setScreenArea(getVisibleRect());

            if (pendingDisplayArea != null) {
                doSetDisplayArea(pendingDisplayArea);
                pendingDisplayArea = null;

            } else if (mapContent.getViewport().getBounds().isEmpty()) {
                setFullExtent();
                doSetDisplayArea(fullExtent);
            }

            publishEvent(
                    new MapPaneEvent(
                            this, MapPaneEvent.Type.DISPLAY_AREA_CHANGED, getDisplayArea()));

            drawLayers(true);
        }

    }

    @Override
    public void addMapPaneListener(MapPaneListener listener) {
        this.mapPaneListeners.add(listener);
    }

    @Override
    public void addMouseListener(MapMouseListener listener) {
        this.mouseEventDispatcher.addMouseListener(listener);
    }

    /**
     * Helper method for {@linkplain #setDisplayArea} which is also called by other methods that
     * want to set the display area without provoking repainting of the display
     *
     * @param envelope
     *            requested display area
     */
    protected void doSetDisplayArea(Envelope envelope) {
        if (mapContent != null) {
            CoordinateReferenceSystem crs = envelope.getCoordinateReferenceSystem();
            if (crs == null) {
                // assume that it is the current CRS
                crs = mapContent.getCoordinateReferenceSystem();
            }

            ReferencedEnvelope refEnv = new ReferencedEnvelope(
                    envelope.getMinimum(0),
                    envelope.getMaximum(0),
                    envelope.getMinimum(1),
                    envelope.getMaximum(1),
                    crs);

            mapContent.getViewport().setBounds(refEnv);

        } else {
            pendingDisplayArea = new ReferencedEnvelope(envelope);
        }

        // Publish the resulting display area with the event
        publishEvent(
                new MapPaneEvent(this, MapPaneEvent.Type.DISPLAY_AREA_CHANGED, getDisplayArea()));
    }

    private void doSetMapContent(MapContent newMapContent) {
        if (mapContent != newMapContent) {

            if (mapContent != null) {
                mapContent.removeMapLayerListListener(this);
                for (Layer layer : mapContent.layers()) {
                    if (layer instanceof ComponentListener) {
                        removeComponentListener((ComponentListener) layer);
                    }
                }
            }

            mapContent = newMapContent;
            this.renderer.setMapContent(newMapContent);

            if (mapContent != null) {
                MapViewport viewport = mapContent.getViewport();
                viewport.setMatchingAspectRatio(true);
                Rectangle rect = getVisibleRect();
                if (!rect.isEmpty()) {
                    viewport.setScreenArea(rect);
                }

                mapContent.addMapLayerListListener(this);
                mapContent.addMapBoundsListener(this);

                if (!mapContent.layers().isEmpty()) {
                    // set all layers as selected by default for the info tool
                    for (Layer layer : mapContent.layers()) {
                        layer.setSelected(true);

                        if (layer instanceof ComponentListener) {
                            addComponentListener((ComponentListener) layer);
                        }
                    }

                    setFullExtent();
                    doSetDisplayArea(mapContent.getViewport().getBounds());
                }
            }

            MapPaneEvent event = new MapPaneEvent(this, MapPaneEvent.Type.NEW_MAPCONTENT, mapContent);
            publishEvent(event);

            drawLayers(false);
        }
    }

    /**
     * Draws layers into one or more images which will then be displayed by the map pane.
     *
     * @param recreate
     */
    protected void drawLayers(boolean recreate) {
        // TODO
        repaint();
    }

    @Override
    public CursorTool getCursorTool() {
        return this.currentCursorTool;
    }

    @Override
    public ReferencedEnvelope getDisplayArea() {
        paramsLock.readLock().lock();
        try {
            if (mapContent != null) {
                return mapContent.getViewport().getBounds();
            } else if (pendingDisplayArea != null) {
                return new ReferencedEnvelope(pendingDisplayArea);
            } else {
                return new ReferencedEnvelope();
            }

        } finally {
            paramsLock.readLock().unlock();
        }
    }

    @Override
    public MapContent getMapContent() {
        paramsLock.readLock().lock();
        try {
            return mapContent;
        } finally {
            paramsLock.readLock().unlock();
        }
    }

    @Override
    public MapMouseEventDispatcher getMouseEventDispatcher() {
        return this.mouseEventDispatcher;
    }

    /** {@inheritDoc} */
    @Override
    public AffineTransform getScreenToWorldTransform() {
        paramsLock.readLock().lock();
        try {
            if (mapContent != null) {
                return mapContent.getViewport().getScreenToWorld();
            } else {
                return null;
            }

        } finally {
            paramsLock.readLock().unlock();
        }
    }

    /** {@inheritDoc} */
    @Override
    public AffineTransform getWorldToScreenTransform() {
        paramsLock.readLock().lock();
        try {
            if (mapContent != null) {
                return mapContent.getViewport().getWorldToScreen();
            } else {
                return null;
            }

        } finally {
            paramsLock.readLock().unlock();
        }
    }

    @Override
    public void layerAdded(MapLayerListEvent event) {
        paramsLock.writeLock().lock();
        try {
            Layer layer = event.getElement();

            if (layer instanceof ComponentListener) {
                super.addComponentListener((ComponentListener) layer);
            }

            setFullExtent();
            MapViewport viewport = this.mapContent.getViewport();
            if (viewport.getBounds().isEmpty()) {
                viewport.setBounds(this.fullExtent);
            }

        } finally {
            paramsLock.writeLock().unlock();
        }

        this.drawLayers(false);
        super.repaint();
    }

    @Override
    public void layerChanged(MapLayerListEvent event) {
        paramsLock.writeLock().lock();
        try {
            int reason = event.getMapLayerEvent().getReason();

            if (reason == MapLayerEvent.DATA_CHANGED) {
                setFullExtent();
            }

            if (reason != MapLayerEvent.SELECTION_CHANGED) {
                clearLabelCache.set(true);
                drawLayers(false);
            }

        } finally {
            paramsLock.writeLock().unlock();
        }

        repaint();
    }

    /** {@inheritDoc} */
    @Override
    public void layerMoved(MapLayerListEvent event) {
        drawLayers(false);
        repaint();
    }

    /** {@inheritDoc} */
    @Override
    public void layerPreDispose(MapLayerListEvent event) {
        // getRenderingExecutor().cancelAll();
    }

    @Override
    public void layerRemoved(MapLayerListEvent event) {
        paramsLock.writeLock().lock();
        try {
            Layer layer = event.getElement();

            if (layer instanceof ComponentListener) {
                removeComponentListener((ComponentListener) layer);
            }

            if (mapContent.layers().isEmpty()) {
                fullExtent = null;
            } else {
                setFullExtent();
            }

        } finally {
            paramsLock.writeLock().unlock();
        }

        drawLayers(false);
        repaint();
    }

    @Override
    public void mapBoundsChanged(MapBoundsEvent event) {
        paramsLock.writeLock().lock();
        try {
            int type = event.getType();
            if ((type & MapBoundsEvent.COORDINATE_SYSTEM_MASK) != 0) {
                /*
                 * The coordinate reference system has changed. Set the map
                 * to display the full extent of layer bounds to avoid the
                 * effect of a shrinking map
                 */
                setFullExtent();
                reset();
            }

        } finally {
            paramsLock.writeLock().unlock();
        }
    }

    @Override
    public void moveImage(int dx, int dy) {
        // TODO Auto-generated method stub

    }

    /**
     * Publish a MapPaneEvent to registered listeners
     *
     * @param ev
     *            the event to publish
     * @see MapPaneListener
     */
    protected void publishEvent(MapPaneEvent ev) {
        for (MapPaneListener listener : mapPaneListeners) {
            switch (ev.getType()) {
            case NEW_MAPCONTENT:
                listener.onNewMapContent(ev);
                break;

            case DISPLAY_AREA_CHANGED:
                listener.onDisplayAreaChanged(ev);
                break;

            case RENDERING_STARTED:
                listener.onRenderingStarted(ev);
                break;

            case RENDERING_STOPPED:
                listener.onRenderingStopped(ev);
                break;
            }
        }
    }

    @Override
    public void removeMapPaneListener(MapPaneListener listener) {
        this.mapPaneListeners.remove(listener);
    }

    @Override
    public void removeMouseListener(MapMouseListener listener) {
        this.mouseEventDispatcher.removeMouseListener(listener);
    }

    @Override
    public void reset() {
        paramsLock.writeLock().lock();
        try {
            if (fullExtent != null) {
                setDisplayArea(fullExtent);
            }

        } finally {
            paramsLock.writeLock().unlock();
        }
    }

    @Override
    public void setBackground(Color bg) {
        super.setBackground(bg);
        if (this.renderer != null)
            this.renderer.setClearColor(bg);
    }

    @Override
    public void setCursorTool(CursorTool tool) {
        paramsLock.writeLock().lock();
        try {
            if (currentCursorTool != null) {
                mouseEventDispatcher.removeMouseListener(currentCursorTool);
            }

            currentCursorTool = tool;

            if (currentCursorTool == null) {
                setCursor(Cursor.getDefaultCursor());
                dragBox.setEnabled(false);

            } else {
                setCursor(currentCursorTool.getCursor());
                dragBox.setEnabled(currentCursorTool.drawDragBox());
                currentCursorTool.setMapPane(this);
                mouseEventDispatcher.addMouseListener(currentCursorTool);
            }

        } finally {
            paramsLock.writeLock().unlock();
        }
    }

    @Override
    public void setDisplayArea(Envelope envelope) {
        paramsLock.writeLock().lock();
        try {
            if (envelope == null) {
                throw new IllegalArgumentException("envelope must not be null");
            }

            doSetDisplayArea(envelope);
            if (mapContent != null) {
                clearLabelCache.set(true);
                drawLayers(false);
            }

        } finally {
            paramsLock.writeLock().unlock();
        }
    }

    /**
     * Determines the full extent of all layers
     *
     * @return {@code true} if full extent was set successfully
     */
    protected boolean setFullExtent() {
        if (mapContent != null && !mapContent.layers().isEmpty()) {
            try {
                fullExtent = mapContent.getMaxBounds();

                /*
                 * Guard against degenerate envelopes (e.g. empty
                 * map layer or single point feature)
                 */
                if (fullExtent == null) {
                    // set arbitrary bounds centred on 0,0
                    fullExtent = new ReferencedEnvelope(
                            -1, 1, -1, 1, mapContent.getCoordinateReferenceSystem());

                } else {
                    double w = fullExtent.getWidth();
                    double h = fullExtent.getHeight();
                    double x = fullExtent.getMinimum(0);
                    double y = fullExtent.getMinimum(1);

                    double xmin = x;
                    double xmax = x + w;
                    if (w <= 0.0) {
                        xmin = x - 1.0;
                        xmax = x + 1.0;
                    }

                    double ymin = y;
                    double ymax = y + h;
                    if (h <= 0.0) {
                        ymin = y - 1.0;
                        ymax = y + 1.0;
                    }

                    fullExtent = new ReferencedEnvelope(
                            xmin,
                            xmax,
                            ymin,
                            ymax,
                            mapContent.getCoordinateReferenceSystem());
                }

            } catch (Exception ex) {
                throw new IllegalStateException(ex);
            }
        } else {
            fullExtent = null;
        }

        return fullExtent != null;
    }

    @Override
    public void setMapContent(MapContent content) {
        paramsLock.writeLock().lock();
        try {
            doSetMapContent(content);
        } finally {
            paramsLock.writeLock().unlock();
        }
    }

    @Override
    public void setMouseEventDispatcher(MapMouseEventDispatcher dispatcher) {
        if (this.mouseEventDispatcher != null) {
            this.mouseEventDispatcher.removeAllListeners();
        }

        this.mouseEventDispatcher = dispatcher;
    }

}
