package org.geotools.map.opengl;

import java.awt.Color;
import java.awt.Rectangle;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.map.Layer;
import org.geotools.map.MapContent;
import org.geotools.map.MapLayerEvent;
import org.geotools.map.MapLayerListEvent;
import org.geotools.map.MapLayerListListener;
import org.geotools.map.MapViewport;
import org.geotools.map.opengl.util.ErrorCheck;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;

public class Renderer implements GLEventListener {
	private static final boolean DEBUG = false;
	private static final boolean STOPWATCH = false;

	private float[] clearColor = { 0.2f, 0.4f, 0.8f, 1f };
	private boolean clearColorChanged = true;
	private MapContent mapContent;
	private ReferencedEnvelope displayArea = new ReferencedEnvelope();
	private MapLayerListListener listener = new LayerListListener();
	private List<RenderableLayer> renderableLayers = new LinkedList<>();
	private Map<Layer, RenderableLayer> layerMap = new HashMap<>();

	@Override
	public void init(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		gl.glEnable(GL2.GL_DEPTH_TEST);

		gl.glEnable(GL2.GL_BLEND);
		gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);

		gl.glClearColor(clearColor[0], clearColor[1], clearColor[2], clearColor[3]);
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		// TODO Auto-generated method stub

	}

	@Override
	public void display(GLAutoDrawable drawable) {
		long start;
		if (STOPWATCH)
			start = System.currentTimeMillis();

		{
			GL2 gl = drawable.getGL().getGL2();

			ReferencedEnvelope mapArea = null;
			if (this.mapContent != null && this.mapContent.getViewport() != null
					&& this.mapContent.getViewport().getScreenArea() != null) {
				Rectangle screenArea = this.mapContent.getViewport().getScreenArea();
				mapArea = this.mapContent.getViewport().getBounds();
				if (screenArea != null) {
					reshape(drawable, screenArea.x, screenArea.y, screenArea.x + screenArea.width,
							screenArea.y + screenArea.height);
				}
			}

			if (this.clearColorChanged) {
				gl.glClearColor(clearColor[0], clearColor[1], clearColor[2], clearColor[3]);
				ErrorCheck.check(gl, "glClearColor");
			}

			gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
			ErrorCheck.check(gl, "glClear");

			drawLayers(gl);
		}
		
		if (STOPWATCH)
			System.out.println((System.currentTimeMillis() - start));
	}

	private void drawLayers(GL2 gl) {
		float scale = (float) this.mapContent.getViewport().getScreenToWorld().getScaleY();
		if (DEBUG)
			System.out.printf("Drawing %d layers\n", this.mapContent.layers().size());
		for (Layer layer : this.mapContent.layers()) {
			RenderableLayer renderableLayer = this.layerMap.get(layer);
			if (renderableLayer == null) {
				if (DEBUG)
					System.out.printf("Missing renderable layer for %s . %s\n", layer.getTitle(), layer.getClass());
			}
			renderableLayer.render(gl, scale);
		}
		if (DEBUG)
			System.out.println("All layers done");
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
		GL2 gl = drawable.getGL().getGL2();

		gl.glViewport(x, y, width, height);

		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		MapViewport viewport = this.mapContent.getViewport();
		if (viewport != null) {
//            AffineTransform worldToScreen = viewport.getWorldToScreen();
//            if (worldToScreen != null) {
//                double[] worldToScreenMatrix = new double[6];
//                worldToScreen.getMatrix(worldToScreenMatrix);
//                float m00 = (float) worldToScreenMatrix[0];
//                float m01 = (float) worldToScreenMatrix[1];
//                float m10 = (float) worldToScreenMatrix[2];
//                float m11 = (float) worldToScreenMatrix[3];
//                float m02 = (float) worldToScreenMatrix[4];
//                float m12 = (float) worldToScreenMatrix[5];
//                float[] matrix = {
//                        m00, m01, m02, 0,
//                        m10, m11, m12, 0,
//                        0, 0, 1, 0,
//                        0, 0, 0, 1 };
//                // gl.glLoadMatrixf(matrix, 0);
//            }

			ReferencedEnvelope mapArea = viewport.getBounds();
			if (mapArea != null) {
				final int layerCount = this.mapContent.layers().size();
				gl.glOrtho(mapArea.getMinX(), mapArea.getMaxX(), mapArea.getMinY(), mapArea.getMaxY(), -layerCount, 1);
				ErrorCheck.check(gl, "glOrtho");
			}

			// Rectangle screenArea = viewport.getScreenArea();
			// if (screenArea != null) {
			// gl.glViewport(screenArea.x, screenArea.x + screenArea.width, screenArea.y,
			// screenArea.y + screenArea.height);
			// }
		}
		gl.glMatrixMode(GL2.GL_MODELVIEW);

		// gl.glMatrixMode(GL2.GL_PROJECTION);
		// gl.glLoadIdentity();
		// gl.glOrthof(456336.529f, 456872.255f, 6156541.554f, 6157411.299f, 1, -1);
		// gl.glMatrixMode(GL2.GL_MODELVIEW);

	}

	public void setDisplayArea(ReferencedEnvelope fullExtent) {
		// TODO Auto-generated method stub
		this.displayArea = fullExtent;
	}

	public void setClearColor(Color color) {
		this.clearColor = color.getRGBColorComponents(clearColor);
	}

	public void setMapContent(MapContent newMapContent) {
		if (this.mapContent != null) {
			this.mapContent.removeMapLayerListListener(this.listener);
		}

		this.mapContent = newMapContent;

		for (RenderableLayer layer : this.layerMap.values()) {
			layer.dispose();
		}
		this.layerMap.clear();

		if (this.mapContent != null) {
			this.mapContent.addMapLayerListListener(this.listener);

			updateLayers();
		}
	}

	private void updateLayers() {
		final int depthIncrement = 1;
		int depth = 0;
		for (Layer layer : this.mapContent.layers()) {
			RenderableLayer rLayer = this.layerMap.get(layer);
			if (rLayer == null) {
				System.out.println("Missing renderable layer for layer: " + layer.getTitle());
				rLayer = new RenderableLayer(this.mapContent, layer, depth);
				this.layerMap.put(layer, rLayer);
			} else {
				rLayer.setDepth(depth);
			}
			depth += depthIncrement;
		}
	}

	private class LayerListListener implements MapLayerListListener {

		@Override
		public void layerAdded(final MapLayerListEvent event) {
			updateLayers();
		}

		@Override
		public void layerRemoved(MapLayerListEvent event) {
			Layer layer = event.getElement();
			Renderer.this.layerMap.remove(layer);
			updateLayers();
		}

		@Override
		public void layerChanged(MapLayerListEvent event) {
			// TODO Auto-generated method stub
			switch (event.getMapLayerEvent().getReason()) {
			case MapLayerEvent.VISIBILITY_CHANGED:
				break;
			case MapLayerEvent.STYLE_CHANGED:
				break;
			case MapLayerEvent.DATA_CHANGED:
				break;
			case MapLayerEvent.FILTER_CHANGED:
				break;
			case MapLayerEvent.METADATA_CHANGED:
				break;
			case MapLayerEvent.SELECTION_CHANGED:
				break;
			case MapLayerEvent.PRE_DISPOSE:
				break;
			default:
				break;
			}
		}

		@Override
		public void layerMoved(MapLayerListEvent event) {
			// TODO Auto-generated method stub

		}

		@Override
		public void layerPreDispose(MapLayerListEvent event) {
			Layer layer = event.getElement();
			RenderableLayer rLayer = Renderer.this.layerMap.remove(layer);
			if (rLayer != null) {
				rLayer.dispose();
			}
		}

	}

}
